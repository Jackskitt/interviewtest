﻿using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Services
{
    public interface IPaymentRequestService
    {
        MakePaymentResult GetAccountResult(MakePaymentRequest request, Account account);
    }
}

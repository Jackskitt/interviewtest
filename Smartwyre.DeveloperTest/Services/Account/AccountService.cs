﻿using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Services
{
    /// <summary>
    /// We want to create this service instead of just injecting the AccountDataStore as we want
    /// to mock the account returning independent of the data store type, i've assumed the AccountDataStore would be an
    /// external library we have no control over so cannot modify the methods or create an interface to mock it
    /// </summary>
    public class AccountService : IAccountService
    {
        public Account GetAccount(string accountNumber)
        {
            var getDataStore = new AccountDataStore();
            return getDataStore.GetAccount(accountNumber);
        }

        public void UpdateAccount(Account account)
        {
            var updateDataStore = new AccountDataStore();
            updateDataStore.UpdateAccount(account);
        }
    }
}

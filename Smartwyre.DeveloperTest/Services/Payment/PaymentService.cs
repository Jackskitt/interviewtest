﻿using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRequestService paymentRequestService;
        private readonly IAccountService accountService;

        public PaymentService(IPaymentRequestService paymentRequestService, IAccountService accountService)
        {
            this.paymentRequestService = paymentRequestService;
            this.accountService = accountService;
        }

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            Account account = accountService.GetAccount(request.DebtorAccountNumber);

            var result = paymentRequestService.GetAccountResult(request, account);

            if (result.Success)
            {
                account.Balance -= request.Amount;
                accountService.UpdateAccount(account);
            }

            return result;
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Types;
using System;

namespace Smartwyre.DeveloperTest.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddScoped<IAccountService, AccountService>()
                .AddScoped<IPaymentRequestService, PaymentRequestService>()
                .AddScoped<IPaymentService, PaymentService>()
                .BuildServiceProvider();

            var paymentService = serviceProvider.GetService<IPaymentService>();
            //We could take the input from the console but i didn't feel it was necessary to do all the validation
            //having static data proves it does run and especially as there are no buisiness requirements in place
            //for what the validation should be

            var makePaymentResponse = paymentService.MakePayment(new MakePaymentRequest
            {
                Amount = 100,
                CreditorAccountNumber = "232",
                DebtorAccountNumber = "322",
                PaymentScheme = PaymentScheme.BankToBankTransfer
            });

            Console.WriteLine("Make Payment Response: " + makePaymentResponse.Success);
            Console.ReadKey();
        }
    }
}

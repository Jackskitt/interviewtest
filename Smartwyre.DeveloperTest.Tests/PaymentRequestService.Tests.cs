using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Types;
using Xunit;

namespace Smartwyre.DeveloperTest.Tests
{
    public class PaymentRequestServiceTests
    {
        /// <summary>
        /// Lets play pretend and pretend i wrote these for all the payment scheme combos,
        /// so i could fit more useful demos into the time
        /// </summary>
        [Fact]
        public void TEST_BANK_TO_BANK_TRANSFER_WITHOUT_FLAG_EXPECT_FAILURE()
        {
            var makePaymentRequest = new MakePaymentRequest
            {
                Amount = 123,
                PaymentScheme = PaymentScheme.BankToBankTransfer
            };

            var account = new Account
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.AutomatedPaymentSystem
            };
            var paymentReqService = new PaymentRequestService();
            var result = paymentReqService.GetAccountResult(makePaymentRequest, account);
            Assert.False(result.Success);
        }

        [Fact]
        public void TEST_BANK_ACCOUNT_NULL_EXPECT_FALSE()
        {
            var makePaymentRequest = new MakePaymentRequest
            {
                Amount = 123,
                PaymentScheme = PaymentScheme.BankToBankTransfer
            };

            var paymentReqService = new PaymentRequestService();
            var result = paymentReqService.GetAccountResult(makePaymentRequest, null);
            Assert.False(result.Success);
        }

        [Fact]
        public void TEST_EXPIDITED_PAYMENTS_BALANCE_LESS_THAN_AMOUNT_EXPECT_FALSE()
        {
            var makePaymentRequest = new MakePaymentRequest
            {
                Amount = 123,
                PaymentScheme = PaymentScheme.ExpeditedPayments
            };
            var account = new Account
            {
                Balance = 30,
                AllowedPaymentSchemes = AllowedPaymentSchemes.ExpeditedPayments
            };
            var paymentReqService = new PaymentRequestService();
            var result = paymentReqService.GetAccountResult(makePaymentRequest, account);
            Assert.False(result.Success);
        }

        [Fact]
        public void TEST_AUTOMATED_PAYMENT_SYSTEM_WITH_NON_LIVE_STATUS_EXPECT_FALSE()
        {
            var makePaymentRequest = new MakePaymentRequest
            {
                Amount = 123,
                PaymentScheme = PaymentScheme.AutomatedPaymentSystem
            };
            var account = new Account
            {
                Balance = 30,
                Status = AccountStatus.Disabled,
                AllowedPaymentSchemes = AllowedPaymentSchemes.AutomatedPaymentSystem
            };
            var paymentReqService = new PaymentRequestService();
            var result = paymentReqService.GetAccountResult(makePaymentRequest, account);
            Assert.False(result.Success);
        }
    }
}

using Moq;
using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Types;
using Xunit;

namespace Smartwyre.DeveloperTest.Tests
{
    public class PaymentServiceTests
    {

        [Fact]
        public void GET_SUCCESSFUL_ACCOUNT_RESULT_EXPECT_CHANGE_OF_BALANCE()
        {
            var accountToReturn = new Account
            {
                Balance = 200
            };
            var mockPaymentRequestService = new Mock<IPaymentRequestService>();

            mockPaymentRequestService.Setup(x => x.GetAccountResult(It.IsAny<MakePaymentRequest>(), It.IsAny<Account>()))
            .Returns(new MakePaymentResult
            {
                Success = true
            });

            var mockAccountDataStore = new Mock<IAccountService>();
            mockAccountDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(accountToReturn);
            var paymentService = new PaymentService(mockPaymentRequestService.Object, mockAccountDataStore.Object);

            var paymentResponse = paymentService.MakePayment(new MakePaymentRequest
            {
                Amount = 100
            });

            Assert.True(paymentResponse.Success);
            Assert.Equal(100, accountToReturn.Balance);
        }

        [Fact]
        public void GET_FAILED_ACCOUNT_RESULT_EXPECT_NO_CHANGE()
        {
            var accountToReturn = new Account
            {
                Balance = 200
            };
            var mockPaymentRequestService = new Mock<IPaymentRequestService>();

            mockPaymentRequestService.Setup(x => x.GetAccountResult(It.IsAny<MakePaymentRequest>(), It.IsAny<Account>()))
            .Returns(new MakePaymentResult
            {
                Success = false
            });

            var mockAccountDataStore = new Mock<IAccountService>();
            mockAccountDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(accountToReturn);
            var paymentService = new PaymentService(mockPaymentRequestService.Object, mockAccountDataStore.Object);

            var paymentResponse = paymentService.MakePayment(new MakePaymentRequest
            {
                Amount = 100
            });

            Assert.False(paymentResponse.Success);
            Assert.Equal(200, accountToReturn.Balance);
        }
    }
}

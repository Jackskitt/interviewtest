using Smartwyre.DeveloperTest.Services;
using Xunit;

namespace Smartwyre.DeveloperTest.Tests
{
    public class AccountServiceTests
    {

        [Fact]
        public void GET_ACCOUNT_EXPECT_NOT_NULL()
        {
            var accountService = new AccountService();
            var fetchedAccount = accountService.GetAccount(null);

            Assert.NotNull(fetchedAccount);
        }
    }
}
